<?php

function isIpAddr($ip) {
    return filter_var($ip, FILTER_VALIDATE_IP);
}

function isKeyVal($key) {
    return is_string($key) && in_array($key, ['csv', 'json', 'xml',
                                              'ip', 'city', 'country',
                                              'hostname', 'loc', 'org',
                                              'postal', 'region']);
}

function uri() {
    $rqs = array_values(array_filter(explode('/', $_SERVER['REQUEST_URI'])));
    $rts = [isset($_SERVER['HTTP_CF_CONNECTING_IP'])
                ? $_SERVER['HTTP_CF_CONNECTING_IP']
                : $_SERVER['REMOTE_ADDR'], 'json'];
    $rls = ['isIpAddr', 'isKeyVal'];

    foreach ($rqs as $rqu) {
        foreach ($rls as $i => $rle) {
            if (call_user_func($rle, $rqu)) {
                $rts[$i] = $rqu;
                break;
            }
        }
    }

    $old = join('/', $rqs);
    $new = join('/', $rts);

    if ($old !== $new) {
        header('Location: /' . $new, true, 302);
        exit;
    }

    return $new;
}

function req($url) {
    $uri = uri();
    $tmp = join('/', ['cache', md5($uri)]);

    if (!file_exists($tmp)) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, join('/', ['ipinfo.io', $uri]));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $out = curl_exec($ch);
        curl_close($ch);
        file_put_contents($tmp, $out);
    }

    return file_get_contents($tmp);
}

$dom = 'ipinfo.io';
$uri = uri();
$url = join('/', [$dom, $uri]);

echo req($url);
